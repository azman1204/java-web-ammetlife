CREATE TABLE employee (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  NAME VARCHAR(50),
  email VARCHAR(50)
);
ALTER TABLE employee ADD age INT;

INSERT INTO employee (NAME, email, age) VALUES('John Doe', 'john.doe@gmail.com', 40);
INSERT INTO employee (NAME, email, age) VALUES('John Smith', 'smith@gmail.com', 42);
INSERT INTO employee (NAME, email, age) VALUES('Abu Bakar Ella', 'abu@gmail.com', 44);

SELECT * FROM employee;

DELIMITER // ;
CREATE PROCEDURE insert_employee(IN myage INT, IN myname VARCHAR(50),IN myemail VARCHAR(50))
BEGIN
INSERT INTO employee(NAME, email, age) VALUES (myname, myemail, myage);
END //

CALL insert_employee(46, 'ali', 'ali@yahoo.com');