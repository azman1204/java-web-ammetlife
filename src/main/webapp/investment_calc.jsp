<%
// check if user logged-in
if (session.getAttribute("loggedin") == null) {
    response.sendRedirect("login.jsp");
}
%>
<%@ include file="./include/header.jsp" %>
<form method="post" action="investment_calc.jsp">
    Investment Amount RM  : <input type="text" name="amount">
    <br>
    Investment Duration (Year) : <input type="text" name="duration">
    <br>
    Profit Rate (%) : <input type="text" name="rate">
    <br>
    <input type="submit" value="Go" class='btn btn-primary'>
</form>

<!-- read input -->
<%
String str = "";
if (request.getParameter("amount") != null) {
    int amount  = Integer.parseInt(request.getParameter("amount"));
    int duration = Integer.parseInt(request.getParameter("duration"));
    float rate      = Float.parseFloat(request.getParameter("rate"));
    for (int year = 1; year <= duration; year++) {
        float profit = amount * rate;
        str = str + "<tr>" +
                            "<td>" + year + "</td>" +
                            "<td>" + amount + "</td>" +
                            "<td>" + (int)profit + "</td>" +
                         "</tr>";
        amount = (int) (amount + profit);
    }
}
%>
<table class='table table-bordered'>
    <tr>
        <td>Year</td>
        <td>Amount</td>
        <td>Profit</td>
    </tr>
    <%= str %>
</table>
<%@ include file="./include/footer.jsp" %>