package training;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet("/employee")
public class EmployeeServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        // read data from database
        response.setContentType("text/html");
        out.print("coming soon..");
        Statement stmt = db.Mysql.getConnection();
        String sql = "SELECT * FROM employee";
        try {
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                out.print(rs.getString("name"));
                out.print("<br>");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
