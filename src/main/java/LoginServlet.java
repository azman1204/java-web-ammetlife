import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import db.Sqlite;

@WebServlet("/auth")
public class LoginServlet extends HttpServlet {
    // run this method auto every time
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("im in service method");
        // localhost:8080/web1/auth?logout
        if (request.getParameter("logout") != null) {
            this.logout(request, response);
        } else {
            this.doPost(request, response);
        }
    }

    public void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession sess = request.getSession();
        sess.invalidate(); //kill session
        RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
        dispatcher.forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String userid = request.getParameter("userid");
        String password = request.getParameter("password");
        PrintWriter out = response.getWriter();
        out.print("userid = " + userid + " password = " + password);

        Statement stmt = Sqlite.getConnection();
        String sql = "SELECT * FROM user WHERE user_id = '" + userid + "' AND password = '" + password + "'";
        boolean ok = false;
        try {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                ok = true;
                System.out.println("User id and password matched!");
            } else {
                System.out.println("Wrong userid and pwd");
            }
        } catch (SQLException e) {
            System.out.println("ERROR 1 : " + e.getMessage());
        }

        //if (userid.equals("user1") && password.equals("1234")) {
        if (ok) {
            // set session
            HttpSession session = request.getSession();
            session.setAttribute("loggedin", true);
            RequestDispatcher dispatcher = request.getRequestDispatcher("investment_calc.jsp");
            dispatcher.forward(request, response);
        } else {
            // redirect back to login page
            RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
            dispatcher.forward(request, response);
        }

    }
}
