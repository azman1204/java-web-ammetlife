package db;

import java.sql.*;

public class Sqlite {
    public static Statement getConnection() {
        Statement stmt = null;
        String path = "C:\\Users\\azman\\Desktop\\javatraining\\sqlite-tools-win32-x86-3400000\\sqlite-tools-win32-x86-3400000\\db.db";
        String url = "jdbc:sqlite:" + path;
        try {
            Connection conn = DriverManager.getConnection(url);
            stmt = conn.createStatement();
        } catch (SQLException e) {
            System.out.println("ERROR " + e.getMessage());
        }
        return stmt;
    }

    public static void main(String[] args) {
        Statement stmt = Sqlite.getConnection();
        String sql = "SELECT * FROM user";
        try {
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                System.out.println(rs.getString("user_id"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
