package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Mysql {
    public static Statement getConnection() {
        Statement stmt = null;
        String url = "jdbc:mysql://167.99.79.74:3306/javase";
        try {
            Connection conn = DriverManager.getConnection(url, "java",
                    "java@2022");
            stmt = conn.createStatement();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return stmt;
    }
}
