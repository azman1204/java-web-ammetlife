 <%@ include file="../include/header.jsp" %>
<%
 session.setAttribute("name", "John Doe");
 session.setAttribute("age", 40);
 Cookie cookie = new Cookie("userid", "1234");
 cookie.setMaxAge(24 * 60 * 60);
 response.addCookie(cookie);
 %>

 Name : <%= session.getAttribute("name") %>
 <br>
 Age    : <%= session.getAttribute("age") %>
<br>
 <a href='sample2.jsp'>Go to another example page</a>
  <%@ include file="../include/footer.jsp" %>