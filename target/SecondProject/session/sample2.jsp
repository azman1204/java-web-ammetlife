 <%@ include file="../include/header.jsp" %>
 <h4>Sample 2</h4>
 Name : <%= session.getAttribute("name") %>
 <br>
 Age    : <%= session.getAttribute("age") %>
 <%
 //read cookies
 Cookie[] cookies = request.getCookies();
 for(Cookie c : cookies) {
    out.print("<hr>");
    out.print(c.getName() + " : ");
    out.print(c.getValue());
 }
 %>
 <%@ include file="../index.jsp" %>
 <%@ include file="../include/footer.jsp" %>