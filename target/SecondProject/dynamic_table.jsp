<%!
// declare method here
public String table(int no) {
    String table = "<table border='1' width='30%'>";
    for (int i=0; i<no; i++) {
        table += "<tr><td>" + i + "</td></tr>";
    }
    table += "</table>";
    return table;
}
%>

<h4>Dynamic Table</h4>
<form method="post" action="dynamic_table.jsp">
    No of Row : <input type="text" name="no">
    <input type="submit" value="Submit">
</form>

<%
int no = 10;
if (request.getParameter("no") != null) {
    //convert string to integer
    no = Integer.parseInt(request.getParameter("no"));
}
%>

<%= table(no) %>